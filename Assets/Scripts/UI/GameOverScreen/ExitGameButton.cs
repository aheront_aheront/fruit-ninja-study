using UnityEngine;
using UnityEngine.UI;

namespace Assets.UI.GameOverScreen
{
    [RequireComponent(typeof(Button))]
    public class ExitGameButton : MonoBehaviour
    {
        private Button _button;

        private void Awake()
        {
            TryGetComponent(out _button);    
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            Application.Quit();
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnClick);
        }
    }
}