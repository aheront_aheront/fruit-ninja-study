using Assets.Game.Player;
using TMPro;
using UnityEngine;

namespace Assets.UI.GameOverScreen
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class NewRecordText : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            _text.enabled = (Score.Instance.CurrentScore == BestScore.Instance.CurrentBestScore);
        }

        private void OnDisable()
        {
            _text.enabled = false;
        }
    }
}