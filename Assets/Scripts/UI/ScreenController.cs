using Assets.Game;
using UnityEngine;

namespace Assets.UI
{
    public class ScreenController : MonoBehaviour
    {
        [SerializeField] private GameObject gameScreen;
        [SerializeField] private GameObject gameOverScreen;

        private void OnEnable()
        {
            GameState.GameStateChanged += OnGameStateChanged;
            OnGameStateChanged();
        }

        private void OnGameStateChanged()
        {
            if (GameState.Instance.State == GameStates.Going)
            {
                gameScreen.SetActive(true);
                gameOverScreen.SetActive(false);
            }
            else if (GameState.Instance.State == GameStates.Ended)
            {
                gameScreen.SetActive(true);
                gameOverScreen.SetActive(true);
            }
        }
    }
}