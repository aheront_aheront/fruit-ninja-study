using Assets.Game.Player;
using TMPro;
using UnityEngine;

namespace Assets.UI.GameScreen.Combo
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    [RequireComponent(typeof(Animation))]
    public class ComboText : MonoBehaviour
    {
        private TextMeshProUGUI _text;
        private Animation _anim;

        private void Awake()
        {
            TryGetComponent(out _text);
            TryGetComponent(out _anim);
        }

        private void Update()
        {
            int combo = ComboCounter.Instance.GetCombo();
            if (combo == 0)
            {
                _text.enabled = false;
            }
            else
            {
                _text.enabled = true;
                _text.text = "x" + combo.ToString();
                _text.color = Color.white;
            }
            if (combo >= 10)
            {
                _text.text += '!';
                _anim.Play();
            }
            if (combo >= 25)
            {
                _text.text += '!';
            }
            if (combo >= 50)
            {
                _text.text += '!';
                _text.color = Color.red;
            }
        }
    }
}