using Assets.Game.Player;
using Assets.UI.GameScreen;
using Assets.UI.GameScreen.Combo;
using UnityEngine;

public class ComboController : MonoBehaviour
{
    [SerializeField] private ComboText _text;
    [SerializeField] private RemainTimeSlider _slider;

    private void Update()
    {
        int combo = ComboCounter.Instance.GetCombo();
        double remainTime = ComboCounter.Instance.GetRemainTimeToReset();
        if (combo == 0 || remainTime < 0)
        {
            _slider.Hide();
            _text.gameObject.SetActive(false);
        }
        else
        {
            _slider.Show();
            _text.gameObject.SetActive(true);
        }
    }
}

