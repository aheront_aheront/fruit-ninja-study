using Assets.Game.Player;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.UI.GameScreen
{
    [RequireComponent(typeof(Slider))]
    public class RemainTimeSlider : MonoBehaviour
    {
        private Slider _slider;

        private void Awake()
        {
            TryGetComponent(out _slider);
        }

        private void Update()
        {
            SetRemainedTime((float)ComboCounter.Instance.GetRemainTimeToReset() / ComboCounter.RESET_DELAY);
        }

        public void SetRemainedTime(float remainTimeFraction)
        {
            _slider.value = remainTimeFraction;
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}