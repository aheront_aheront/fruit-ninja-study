using Assets.Game.Player;
using TMPro;
using UnityEngine;

namespace Assets.UI.GameScreen.Score
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class BestScoreText : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            BestScore.BestScoreChanged += OnBestScoreChanged;
            OnBestScoreChanged();
        }

        private void OnBestScoreChanged()
        {
            _text.text = $"������ ����: {BestScore.Instance.CurrentBestScore}";
        }

        private void OnDisable()
        {
            BestScore.BestScoreChanged -= OnBestScoreChanged;
        }
    }
}