using TMPro;
using UnityEngine;

namespace Assets.UI.GameScreen.Score
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ScoreText : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            Game.Player.Score.Instance.ScoreChanged += OnScoreChanged;
            OnScoreChanged();
        }

        private void OnScoreChanged()
        {
            _text.text = $"����: {Game.Player.Score.Instance.CurrentScore}";
        }

        private void OnDisable()
        {
            Game.Player.Score.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}