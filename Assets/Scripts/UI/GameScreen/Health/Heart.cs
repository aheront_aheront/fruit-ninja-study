using UnityEngine;
using UnityEngine.UI;

namespace Assets.UI.GameScreen.Health
{
    [RequireComponent(typeof(Image))]
    public class Heart : MonoBehaviour
    {
        private Image _image;

        private void Awake()
        {
            TryGetComponent(out _image);
        }

        public void Show()
        {
            _image.enabled = true;
        }

        public void Hide()
        {
            _image.enabled = false;
        }
    }
}