using System.Collections.Generic;
using UnityEngine;

namespace Assets.UI.GameScreen.Health
{
    public class Health : MonoBehaviour
    {
        private List<Heart> heartList = new List<Heart>();

        private void Awake()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                if (child.GetComponent<Heart>() != null)
                {
                    heartList.Add(child.GetComponent<Heart>());
                }
            }
        }

        private void OnEnable()
        {
            Game.Player.Health.Instance.HealthChanged += OnHealthChanged;
        }

        private void OnHealthChanged()
        {
            int health = Game.Player.Health.Instance.CurrentHealth;
            for (int i = 0; i < heartList.Count; i++)
            {
                if (i < health)
                {
                    heartList[i].Show();
                }
                else
                {
                    heartList[i].Hide();
                }
            }
        }

        private void OnDisable()
        {
            Game.Player.Health.Instance.HealthChanged -= OnHealthChanged;
        }
    }
}