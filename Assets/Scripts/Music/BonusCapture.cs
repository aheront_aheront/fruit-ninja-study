using Assets.Game.Bonuses.Heal;
using Assets.Game.Bonuses.TimeStop;
using UnityEngine;

namespace Assets.Music
{
    [RequireComponent(typeof(AudioSource))]
    public class BonusCapture : MonoBehaviour
    {
        private AudioSource _audio;

        private void Awake()
        {
            TryGetComponent(out _audio);
        }

        private void OnEnable()
        {
            HealingHeart.HealingHeartSliced += OnBonusCaptured;
            TimeStop.TimeStopSliced += OnBonusCaptured;
        }

        private void OnBonusCaptured()
        {
            _audio.Play();
        }

        private void OnDisable()
        {
            HealingHeart.HealingHeartSliced -= OnBonusCaptured;
            TimeStop.TimeStopSliced -= OnBonusCaptured;
        }
    }
}