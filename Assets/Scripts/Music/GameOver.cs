using Assets.Game;
using UnityEngine;

namespace Assets.Music
{
    [RequireComponent(typeof(AudioSource))]
    public class GameOver : MonoBehaviour
    {
        private AudioSource _audio;

        private void Awake()
        {
            TryGetComponent(out _audio);
        }

        private void OnEnable()
        {
            GameState.GameStateChanged += OnGameStateChanged;
        }

        private void OnGameStateChanged()
        {
            if (GameState.Instance.State == GameStates.Ended)
            {
                _audio.Play();
            }
        }

        private void OnDisable()
        {
            GameState.GameStateChanged -= OnGameStateChanged;
        }
    }
}