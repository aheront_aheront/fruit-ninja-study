using Assets.Game;
using UnityEngine;

namespace Assets.Music
{
    [RequireComponent(typeof(AudioSource))]
    public class Background : MonoBehaviour
    {
        private AudioSource _audio;

        private void Awake()
        {
            TryGetComponent(out _audio);
        }

        private void OnEnable()
        {
            GameState.GameStateChanged += OnGameStateChanged;
        }

        private void OnGameStateChanged()
        {
            if (GameState.Instance.State == GameStates.Going)
            {
                _audio.Play();
            }
            else
            {
                _audio.Stop();
            }
        }

        private void OnDisable()
        {
            GameState.GameStateChanged -= OnGameStateChanged;
        }
    }
}