using Assets.Game.Bombs;
using UnityEngine;

namespace Assets.Music
{
    [RequireComponent(typeof(AudioSource))]
    public class BombExplosion : MonoBehaviour
    {
        private AudioSource _audio;

        private void Awake()
        {
            TryGetComponent(out _audio);
        }

        private void OnEnable()
        {
            Bomb.BombSliced += OnBombSliced;
        }

        private void OnBombSliced()
        {
            _audio.Play();
        }

        private void OnDisable()
        {
            Bomb.BombSliced -= OnBombSliced;
        }
    }
}