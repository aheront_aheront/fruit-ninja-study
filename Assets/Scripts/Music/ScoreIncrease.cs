using Assets.Game.Player;
using UnityEngine;

namespace Assets.Music
{
    [RequireComponent(typeof(AudioSource))]
    public class ScoreIncrease : MonoBehaviour
    {
        private AudioSource _audio;

        private void Awake()
        {
            TryGetComponent(out _audio);
        }

        private void OnEnable()
        {
            Score.Instance.ScoreChanged += OnScoreChanged;
        }

        private void OnScoreChanged()
        {
            _audio.Play();
        }

        private void OnDisable()
        {
            Score.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}