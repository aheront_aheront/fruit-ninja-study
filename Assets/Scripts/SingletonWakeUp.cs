using Assets.Game;
using Assets.Game.Player;
using UnityEngine;

public class SingletonWakeUp : MonoBehaviour
{
    private void Awake()
    {
        object _;
        _ = GameState.Instance;


        _ = BestScore.Instance;
        _ = ComboCounter.Instance;
        _ = DifficultyLevel.Instance;
        _ = Health.Instance;
        _ = Score.Instance;
        _ = null;

        Destroy(gameObject);
    }
}
