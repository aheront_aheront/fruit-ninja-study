using Assets.Game.Slicer;
using System;
using UnityEngine;

namespace Assets.Game.Bombs
{
    public class Bomb : MonoBehaviour, ISliceable
    {
        public static event Action BombSliced;
        [SerializeField] private float _lifetimeSecs;

        private Rigidbody _mainRigidbody;
        private float _lifetimeLeft;

        public void Slice(Vector3 direction, Vector3 position, float force)
        {
            Destroy(gameObject);


            BombSliced?.Invoke();
        }

        public void Throw(float forceMagnitude)
        {
            Vector3 forceVector = transform.up * forceMagnitude;
            _mainRigidbody.AddForce(forceVector, ForceMode.Impulse);
            _lifetimeLeft = _lifetimeSecs;
        }

        private void Awake()
        {
            TryGetComponent(out _mainRigidbody);
        }

        private void OnEnable()
        {
            GameState.GameRestarted += OnRestart;
        }

        private void Update()
        {
            UpdateLifetime();
        }

        private void UpdateLifetime()
        {
            _lifetimeLeft -= Time.deltaTime;
            if (_lifetimeLeft <= 0)
                Destroy(gameObject);
        }

        private void OnRestart()
        {
            Destroy(gameObject);
        }

        private void OnDisable()
        {
            GameState.GameRestarted -= OnRestart;
        }
    }
}