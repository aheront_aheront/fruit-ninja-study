using Assets.Game.Player;
using UnityEngine;

namespace Assets.Game.Bombs
{
    [RequireComponent(typeof(BombsSpawner))]
    public class BombSpawnTimer : MonoBehaviour
    {
        [SerializeField] private float _minDelaySecsNormalDifficulty;
        [SerializeField] private float _maxDelaySecsNormalDifficulty;
        private const float EASY_LEVEL_TIME_COEF = 2f;
        private const float HARD_LEVEL_TIME_COEF = 0.6f;

        private float _timeToNextSpawn = 0;
        private bool _running = false;

        private BombsSpawner _spawner;

        public void StartSpawning()
        {
            _running = true;
        }

        public void StopSpawning()
        {
            _running = false;
        }

        private void Awake()
        {
            TryGetComponent(out _spawner);
            ResetTimer();
        }

        private void OnEnable()
        {
            StartSpawning();
            GameState.GameStateChanged += OnGameStateChanged;
            OnGameStateChanged();
        }

        private void OnGameStateChanged()
        {
            if (GameState.Instance.State == GameStates.Going)
                StartSpawning();
            else
                StopSpawning();
        }

        private void Update()
        {
            UpdateTimer();
        }

        private void UpdateTimer()
        {
            if (!_running) return;
            _timeToNextSpawn = Mathf.Max(_timeToNextSpawn - Time.deltaTime, 0);
            if (_timeToNextSpawn <= 0)
            {
                ResetTimer();
                _spawner.ThrowBomb();
            }
        }

        private void ResetTimer()
        {
            _timeToNextSpawn = Random.Range(_minDelaySecsNormalDifficulty, _maxDelaySecsNormalDifficulty);
            if (DifficultyLevel.Instance.CurrentDifficulty == Difficulty.Easy)
            {
                _timeToNextSpawn *= EASY_LEVEL_TIME_COEF;
            }
            else if (DifficultyLevel.Instance.CurrentDifficulty == Difficulty.Hard)
            {
                _timeToNextSpawn *= HARD_LEVEL_TIME_COEF;
            }
        }

        private void OnDisable()
        {
            StopSpawning();
            GameState.GameStateChanged -= OnGameStateChanged;
        }
    }
}