using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Bombs
{
    [RequireComponent(typeof(Collider))]
    public class BombsSpawner : MonoBehaviour
    {
        [SerializeField] private List<Bomb> BombPrefabs;

        [SerializeField] private float _minAngleZ;
        [SerializeField] private float _maxAngleZ;
        [SerializeField] private float _minForce;
        [SerializeField] private float _maxForce;

        private Collider _spawnZone;

        private void Awake()
        {
            TryGetComponent(out _spawnZone);
        }

        public void ThrowBomb()
        {
            Vector3 startPosition = new Vector3(
                Random.Range(_spawnZone.bounds.min.x, _spawnZone.bounds.max.x),
                Random.Range(_spawnZone.bounds.min.y, _spawnZone.bounds.min.y),
                Random.Range(_spawnZone.bounds.min.z, _spawnZone.bounds.max.z)
            );
            Quaternion startRotation = Quaternion.Euler(0, 0, Random.Range(_minAngleZ, _maxAngleZ));
            Bomb randomPrefab = BombPrefabs[Random.Range(0, BombPrefabs.Count)];
            Bomb bomb = Instantiate(randomPrefab, startPosition, startRotation, transform);
            bomb.Throw(Random.Range(_minForce, _maxForce));
        }

    }
}