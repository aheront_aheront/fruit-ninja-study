using UnityEngine;

namespace Assets.Game.Bombs
{
    [RequireComponent(typeof(Animation))]
    public class BombFlash : MonoBehaviour
    {
        private Animation _anim;

        private void Awake()
        {
            TryGetComponent(out _anim);
        }

        private void OnEnable()
        {
            Bomb.BombSliced += OnBombSliced;
        }

        private void OnBombSliced()
        {
            _anim.Play();
        }

        private void OnDisable()
        {
            Bomb.BombSliced -= OnBombSliced;
        }
    }
}