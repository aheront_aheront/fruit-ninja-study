using Assets.Game.Bonuses.TimeStop;
using System;
using UnityEngine;

namespace Assets.Game
{
    public class TimeController : MonoBehaviour
    {

        private static TimeController _instance;
        public const float TIME_STOP_DURABILITY = 10f;
        private const float TIME_STOP_COEF = 0.5f;
        private float _remainingTimeToTimeSpeedReset = 0f;

        private void OnEnable()
        {
            TimeStop.TimeStopSliced += OnTimeStopSliced;
            GameState.GameRestarted += OnRestart;
        }

        private void OnTimeStopSliced()
        {
            if (_remainingTimeToTimeSpeedReset == 0)
            {
                _remainingTimeToTimeSpeedReset = TIME_STOP_DURABILITY;
                SetTimeScale(TIME_STOP_COEF);
            }
        }

        private void Update()
        {
            _remainingTimeToTimeSpeedReset = Math.Max(0, _remainingTimeToTimeSpeedReset - Time.fixedUnscaledDeltaTime);
            if (_remainingTimeToTimeSpeedReset == 0)
            {
                SetTimeScale(1);
            }
        }

        private void SetTimeScale(float scale)
        {
            if (Time.timeScale == scale) return;
            Time.timeScale = scale;
            Time.fixedDeltaTime = Time.fixedUnscaledDeltaTime * scale;
        }

        private void OnRestart()
        {
            SetTimeScale(1);
        }

        private void OnDisable()
        {
            TimeStop.TimeStopSliced -= OnTimeStopSliced;
            GameState.GameRestarted -= OnRestart;
        }
    }
}