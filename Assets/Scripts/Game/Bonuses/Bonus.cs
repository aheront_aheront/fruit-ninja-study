using UnityEngine;

public abstract class Bonus : MonoBehaviour
{
    public abstract void Throw(float force);
}
