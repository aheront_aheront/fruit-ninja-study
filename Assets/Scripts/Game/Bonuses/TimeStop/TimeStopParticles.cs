using UnityEngine;

namespace Assets.Game.Bonuses.TimeStop
{
    public class TimeStopParticles : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _particles;

        private void OnEnable()
        {
            TimeStop.TimeStopSliced += OnTimeStopSliced;
        }

        private void OnTimeStopSliced()
        {
            Instantiate(_particles, transform.position, transform.rotation);
        }

        private void OnDisable()
        {
            TimeStop.TimeStopSliced -= OnTimeStopSliced;
        }
    }
}