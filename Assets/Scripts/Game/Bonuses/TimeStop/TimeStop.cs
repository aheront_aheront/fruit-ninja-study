using Assets.Game.Slicer;
using System;
using UnityEngine;

namespace Assets.Game.Bonuses.TimeStop
{
    public class TimeStop : Bonus, ISliceable
    {
        public static event Action TimeStopSliced;
        [SerializeField] private float _lifetimeSecs;

        private Rigidbody _mainRigidbody;
        private float _lifetimeLeft;

        public void Slice(Vector3 direction, Vector3 position, float force)
        {
            if(GameState.Instance.State == GameStates.Going)
            {
                TimeStopSliced?.Invoke();
            }
            Destroy(gameObject);
        }

        public override void Throw(float forceMagnitude)
        {
            Vector3 forceVector = transform.up * forceMagnitude;
            _mainRigidbody.AddForce(forceVector, ForceMode.Impulse);
            _lifetimeLeft = _lifetimeSecs;
        }

        private void Awake()
        {
            TryGetComponent(out _mainRigidbody);
        }

        private void OnEnable()
        {
            GameState.GameRestarted += OnRestart;
        }

        private void OnRestart()
        {
            Destroy(gameObject);
        }
        private void Update()
        {
            UpdateLifetime();
        }

        private void UpdateLifetime()
        {
            _lifetimeLeft -= Time.deltaTime;
            if (_lifetimeLeft <= 0)
                Destroy(gameObject);
        }

        private void OnDisable()
        {
            GameState.GameRestarted -= OnRestart;
        }
    }
}