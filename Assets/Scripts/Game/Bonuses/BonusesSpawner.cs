using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Bonuses
{
    [RequireComponent(typeof(Collider))]
    public class BonusesSpawner : MonoBehaviour
    {
        [SerializeField] private List<Bonus> BonusPrefabs;

        [SerializeField] private float _minAngleZ;
        [SerializeField] private float _maxAngleZ;
        [SerializeField] private float _minForce;
        [SerializeField] private float _maxForce;

        private Collider _spawnZone;

        private void Awake()
        {
            TryGetComponent(out _spawnZone);
        }

        public void ThrowBonus()
        {
            Bonus randomPrefab = BonusPrefabs[Random.Range(0, BonusPrefabs.Count)];
            Bonus bonus = Instantiate(randomPrefab, transform);
            Vector3 startPosition = new Vector3(
                Random.Range(_spawnZone.bounds.min.x, _spawnZone.bounds.max.x),
                Random.Range(_spawnZone.bounds.min.y, _spawnZone.bounds.max.y),
                Random.Range(_spawnZone.bounds.min.z, _spawnZone.bounds.max.z)
            );
            bonus.transform.position = startPosition;
            Quaternion startRotation = Quaternion.Euler(
                0,
                0,
                Random.Range(_minAngleZ, _maxAngleZ)
            );
            bonus.transform.rotation = startRotation;
            bonus.Throw(Random.Range(_minForce, _maxForce));
        }

    }
}