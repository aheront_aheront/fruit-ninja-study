using UnityEngine;

namespace Assets.Game.Bonuses
{
    [RequireComponent(typeof(BonusesSpawner))]
    public class BonusesSpawnTimer : MonoBehaviour
    {
        [SerializeField] private float minDelaySecs;
        [SerializeField] private float maxDelaySecs;

        private float _timeToNextSpawn = 0;
        private bool _running = false;

        private BonusesSpawner _spawner;

        private void Awake()
        {
            TryGetComponent(out _spawner);
            ResetTimer();
        }

        public void StartSpawning()
        {
            _running = true;
        }

        public void StopSpawning()
        {
            _running = false;
        }

        private void OnEnable()
        {
            StartSpawning();
            GameState.GameStateChanged += OnGameStateChanged;
            OnGameStateChanged();
        }

        private void OnGameStateChanged()
        {
            if (GameState.Instance.State == GameStates.Going)
                StartSpawning();
            else
                StopSpawning();
        }

        private void Update()
        {
            UpdateTimer();
        }

        private void UpdateTimer()
        {
            if (!_running) return;
            _timeToNextSpawn = Mathf.Max(_timeToNextSpawn - Time.deltaTime, 0);
            if (_timeToNextSpawn <= 0)
            {
                ResetTimer();
                _spawner.ThrowBonus();
            }
        }

        private void ResetTimer()
        {
            _timeToNextSpawn = Random.Range(minDelaySecs, maxDelaySecs);
        }

        private void OnDisable()
        {
            StopSpawning();
            GameState.GameStateChanged -= OnGameStateChanged;
        }
    }
}