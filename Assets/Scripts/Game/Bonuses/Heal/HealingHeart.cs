using Assets.Game.Slicer;
using System;
using UnityEngine;

namespace Assets.Game.Bonuses.Heal
{
    [RequireComponent(typeof(Rigidbody))]
    public class HealingHeart : Bonus, ISliceable
    {
        public static event Action HealingHeartSliced;
        [SerializeField] private float _lifetimeSecs;

        private float _lifetimeLeft;
        private Rigidbody _mainRigidbody;

        public override void Throw(float forceMagnitude)
        {
            Vector3 forceVector = transform.up * forceMagnitude;
            _mainRigidbody.AddForce(forceVector, ForceMode.Impulse);
            _lifetimeLeft = _lifetimeSecs;
        }

        private void Awake()
        {
            TryGetComponent(out _mainRigidbody);
            transform.GetChild(0).GetComponent<HeartModel>().HeartSliced += OnHeartSliced;
        }

        private void OnEnable()
        {
            GameState.GameRestarted += OnRestart;
        }

        public void OnHeartSliced(Vector3 direction, Vector3 position, float force)
        {
            if (GameState.Instance.State == GameStates.Going)
            {
                HealingHeartSliced?.Invoke();
            }
            Destroy(gameObject);
        }

        private void Update()
        {
            UpdateLifetime();
        }

        private void UpdateLifetime()
        {
            _lifetimeLeft -= Time.deltaTime;
            if (_lifetimeLeft <= 0)
                Destroy(gameObject);
        }

        private void OnRestart()
        {
            Destroy(gameObject);
        }

        public void Slice(Vector3 direction, Vector3 position, float force)
        {
            HealingHeartSliced?.Invoke();
            Destroy(gameObject);
        }

        private void OnDisable()
        {
            GameState.GameRestarted -= OnRestart;
        }
    }
}