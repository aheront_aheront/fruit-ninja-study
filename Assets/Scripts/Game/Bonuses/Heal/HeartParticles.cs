using UnityEngine;

namespace Assets.Game.Bonuses.Heal
{
    public class HeartParticles : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _heartParticles;

        private void OnEnable()
        {
            HealingHeart.HealingHeartSliced += OnHeartSliced;
        }

        private void OnHeartSliced()
        {
            Instantiate(_heartParticles, transform.position, transform.rotation);
        }

        private void OnDisable()
        {
            HealingHeart.HealingHeartSliced -= OnHeartSliced;
        }
    }
}