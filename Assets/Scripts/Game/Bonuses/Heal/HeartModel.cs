using Assets.Game.Slicer;
using System;
using UnityEngine;

namespace Assets.Game.Bonuses.Heal
{
    public class HeartModel : MonoBehaviour, ISliceable
    {
        public event Action<Vector3, Vector3, float> HeartSliced;

        public void Slice(Vector3 direction, Vector3 position, float force)
        {
            HeartSliced?.Invoke(direction, position, force);
        }
    }
}