using Assets.Game.Player;
using System;

namespace Assets.Game
{
    public enum GameStates
    {
        Going,
        Ended
    }

    public class GameState
    {
        public static event Action GameStateChanged;
        public static event Action GameRestarted;
        public GameStates State
        {
            get => _gameState;
            private set
            {
                if (_gameState == value) return;
                _gameState = value;
                GameStateChanged?.Invoke();
            }
        }
        public static GameState Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameState();
                return _instance;
            }
        }

        private static GameState _instance;
        private GameStates _gameState = GameStates.Going;

        public GameState()
        {
            Health.Instance.HealthChanged += OnHealthChanged;
        }

        public void Restart()
        {
            State = GameStates.Going;
            GameRestarted?.Invoke();
        }
        private void OnHealthChanged()
        {
            int health = Health.Instance.CurrentHealth;
            if (health <= 0)
                State = GameStates.Ended;
        }

        ~GameState()
        {
            Health.Instance.HealthChanged -= OnHealthChanged;
        }
    }
}