using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Game.Slicer
{
    [RequireComponent(typeof(Collider))]
    public class Slicer : MonoBehaviour
    {
        [SerializeField] private float _sliceForce;
        [SerializeField] private float _minSpeedForSlice;
        private Collider _slicerTrigger;
        private Camera _mainCamera;
        private Vector3 _direction;

        private void Awake()
        {
            TryGetComponent(out _slicerTrigger);
            _mainCamera = Camera.main;
            _slicerTrigger.enabled = false;
        }

        private void Update()
        {
            UpdateSlicing();
        }

        private void UpdateSlicing()
        {
            if (Input.GetMouseButton((int)MouseButton.Left))
            {
                Vector3 mousePosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
                mousePosition.z = transform.position.z;
                _direction = mousePosition - transform.position;
                transform.position = mousePosition;

                bool isSlicing = _direction.magnitude / Time.deltaTime >= _minSpeedForSlice;
                _slicerTrigger.enabled = isSlicing;
            }
            else
            {
                _slicerTrigger.enabled = false;
            }
        }

        private void OnTriggerEnter(Collider fruitCollider)
        {
            ISliceable fruit = fruitCollider.GetComponent<ISliceable>();
            if (fruit == null) return;
            Debug.Log("Sliced!");
            fruit.Slice(_direction, transform.position, _sliceForce);
        }
    }
}