using UnityEngine;

namespace Assets.Game.Slicer
{
    public interface ISliceable
    {
        public void Slice(Vector3 direction, Vector3 position, float force);
    }
}