using System;

namespace Assets.Game.Player
{
    public enum Difficulty
    {
        Easy,
        Normal,
        Hard,
    }
    public class DifficultyLevel
    {
        public event Action LevelChanged;
        public static DifficultyLevel Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DifficultyLevel();
                return _instance;
            }
        }

        public Difficulty CurrentDifficulty
        {
            get => _difficulty;
            set
            {
                if (_difficulty == value) return;
                _difficulty = value;
                LevelChanged?.Invoke();
            }
        }

        private static DifficultyLevel _instance;
        private Difficulty _difficulty;
        private const int NORMAL_LEVEL_REQUIREMENT = 1000;
        private const int HARD_LEVEL_REQUIREMENT = 10000;

        public DifficultyLevel()
        {
            Score.Instance.ScoreChanged += OnScoreChanged;
            OnScoreChanged();
        }

        private void OnScoreChanged()
        {
            int score = Score.Instance.CurrentScore;
            if (score < NORMAL_LEVEL_REQUIREMENT)
            {
                CurrentDifficulty = Difficulty.Easy;
            }
            else if (score < HARD_LEVEL_REQUIREMENT)
            {
                CurrentDifficulty = Difficulty.Normal;
            }
            else
            {
                CurrentDifficulty = Difficulty.Hard;
            }
        }

        ~DifficultyLevel()
        {
            Score.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}