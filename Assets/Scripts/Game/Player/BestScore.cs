using System;
using UnityEngine;

namespace Assets.Game.Player
{
    public class BestScore
    {
        public static event Action BestScoreChanged;

        public static BestScore Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BestScore();
                return _instance;
            }
        }

        public int CurrentBestScore
        {
            get => _bestScore;
            private set
            {
                if (_bestScore == value) return;
                _bestScore = value;
                BestScoreChanged?.Invoke();
            }
        }

        private const string BEST_SCORE_KEY = "Best score";
        private static BestScore _instance;
        private int _bestScore;

        public BestScore()
        {
            CurrentBestScore = PlayerPrefs.GetInt(BEST_SCORE_KEY, Score.Instance.CurrentScore);
            Score.Instance.ScoreChanged += OnScoreChanged;

        }

        private void OnScoreChanged()
        {
            CurrentBestScore = Math.Max(CurrentBestScore, Score.Instance.CurrentScore);
            PlayerPrefs.SetInt(BEST_SCORE_KEY, CurrentBestScore);
            PlayerPrefs.Save();
        }

        ~BestScore()
        {
            Score.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}