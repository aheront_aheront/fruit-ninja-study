using Assets.Game.Fruits;
using UnityEngine;

namespace Assets.Game.Player
{
    public class ComboCounter
    {
        public static ComboCounter Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ComboCounter();
                return _instance;
            }
        }

        private static ComboCounter _instance;

        public const float RESET_DELAY = 1.5f;
        private double _lastCutTime = -RESET_DELAY;
        private int _combo;
        public ComboCounter()
        {
            Fruit.FruitSliced += OnFruitSliced;
            Health.Instance.HealthChanged += OnHealthChanged;
            GameState.GameRestarted += OnGameRestarted;
        }

        public int GetCombo()
        {
            RefreshCombo();
            return _combo;
        }

        public double GetRemainTimeToReset()
        {
            RefreshCombo();
            return RESET_DELAY + _lastCutTime - Time.fixedTime;
        }

        private void OnFruitSliced(Fruit fruit)
        {
            RefreshCombo();
            _combo += 1;
            _lastCutTime = Time.fixedTimeAsDouble;
        }

        private void OnHealthChanged()
        {
            _combo = 0;
        }

        private void OnGameRestarted()
        {
            _combo = 0;
        }
        private void RefreshCombo()
        {
            if (Time.fixedTimeAsDouble - _lastCutTime >= RESET_DELAY)
            {
                _combo = 0;
            }
        }

        ~ComboCounter()
        {
            Fruit.FruitSliced -= OnFruitSliced;
        }
    }
}