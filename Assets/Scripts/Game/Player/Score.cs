using Assets.Game.Fruits;
using System;

namespace Assets.Game.Player
{
    public class Score
    {
        public static Score Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new();
                return _instance;
            }
        }
        public event Action ScoreChanged;
        public int CurrentScore
        {
            get => _score;
            private set
            {
                if (_score == value) return;
                _score = value;
                ScoreChanged?.Invoke();
            }
        }

        private static Score _instance;
        private int _score;

        private Score()
        {
            Fruit.FruitSliced += OnFruitSliced;
            GameState.GameRestarted += OnRestart;
        }

        private void OnFruitSliced(Fruit fruit)
        {
            if (GameState.Instance.State == GameStates.Going)
                CurrentScore += Math.Max(ComboCounter.Instance.GetCombo(), 1);
        }

        private void OnRestart()
        {
            CurrentScore = 0;
        }

        ~Score()
        {
            Fruit.FruitSliced -= OnFruitSliced;
            GameState.GameRestarted -= OnRestart;
        }
    }
}