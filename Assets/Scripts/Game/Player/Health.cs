using Assets.Game.Bombs;
using Assets.Game.Bonuses.Heal;
using Assets.Game.Fruits;
using System;
using UnityEngine;

namespace Assets.Game.Player
{
    public class Health
    {
        public event Action HealthChanged;
        public static Health Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Health();
                return _instance;
            }
        }
        public int CurrentHealth
        {
            get => _health;
            private set
            {
                value = Mathf.Clamp(value, 0, 3);
                if (value == _health)
                    return;
                _health = value;
                HealthChanged?.Invoke();
            }
        }

        private static Health _instance;
        private const int START_HEALTH = 3;

        private int _health = START_HEALTH;

        public Health()
        {
            Fruit.FruitDestroyed += OnFruitDestroyed;
            Bomb.BombSliced += OnBombSliced;
            HealingHeart.HealingHeartSliced += OnHealingHeartSliced;
            GameState.GameRestarted += OnRestart;
        }
        private void OnFruitDestroyed(Fruit fruit)
        {
            if (fruit.State != FruitState.Sliced)
                CurrentHealth--;
        }

        private void OnBombSliced()
        {
            CurrentHealth--;
        }

        private void OnHealingHeartSliced()
        {
            CurrentHealth++;
        }

        private void OnRestart()
        {
            CurrentHealth = START_HEALTH;
        }

        ~Health()
        {
            Fruit.FruitDestroyed -= OnFruitDestroyed;
            Bomb.BombSliced -= OnBombSliced;
            HealingHeart.HealingHeartSliced -= OnHealingHeartSliced;
            GameState.GameRestarted -= OnRestart;
        }
    }
}