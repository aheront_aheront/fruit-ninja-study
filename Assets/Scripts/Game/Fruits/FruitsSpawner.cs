using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Fruits
{
    [RequireComponent(typeof(Collider))]
    public class FruitsSpawner : MonoBehaviour
    {
        [SerializeField] private List<Fruit> FruitPrefabs;

        [SerializeField] private float _minAngleZ;
        [SerializeField] private float _maxAngleZ;
        [SerializeField] private float _minForce;
        [SerializeField] private float _maxForce;

        private Collider _spawnZone;

        private void Awake()
        {
            TryGetComponent(out _spawnZone);
        }

        public void ThrowFruit()
        {
            Vector3 startPosition = new Vector3(
                Random.Range(_spawnZone.bounds.min.x, _spawnZone.bounds.max.x),
                Random.Range(_spawnZone.bounds.min.y, _spawnZone.bounds.min.y),
                Random.Range(_spawnZone.bounds.min.z, _spawnZone.bounds.max.z)
            );
            Quaternion startRotation = Quaternion.Euler(0, 0, Random.Range(_minAngleZ, _maxAngleZ));
            Fruit randomPrefab = FruitPrefabs[Random.Range(0, FruitPrefabs.Count)];
            Fruit fruit = Instantiate(randomPrefab, startPosition, startRotation, transform);
            fruit.Throw(Random.Range(_minForce, _maxForce));
        }
    }
}