using Assets.Game.Slicer;
using System;
using UnityEngine;

namespace Assets.Game.Fruits
{
    public enum FruitState
    {
        Initialized,
        Thrown,
        Sliced
    }

    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class Fruit : MonoBehaviour, ISliceable
    {
        public static event Action<Fruit> FruitSliced;
        public static event Action<Fruit> FruitDestroyed;
        public FruitState State
        {
            get => _state;
            set
            {
                if (_state == value) return;
                _state = value;
                RefreshFruitModel();
            }
        }

        [SerializeField] private GameObject _wholeModel;
        [SerializeField] private GameObject _slicedModel;
        [SerializeField] private Rigidbody _topPartRigidbody;
        [SerializeField] private Rigidbody _bottomPartRigidbody;
        [SerializeField] private float _lifetimeSecs;

        private Rigidbody _mainRigidbody;
        private Collider _sliceTrigger;
        private FruitState _state = FruitState.Initialized;
        private float _lifetimeLeft;

        public void Slice(Vector3 direction, Vector3 position, float force)
        {
            State = FruitState.Sliced;


            float angle = Mathf.Acos(direction.x / direction.magnitude) * Mathf.Rad2Deg;
            if (direction.y < 0)
                angle *= -1;
            _slicedModel.transform.rotation = Quaternion.Euler(0, 0, angle);

            AddForceForSlicedPart(_topPartRigidbody, direction, position, force);
            AddForceForSlicedPart(_bottomPartRigidbody, -direction, position, force);
            FruitSliced?.Invoke(this);
        }

        public void Throw(float forceMagnitude)
        {
            Vector3 forceVector = transform.up * forceMagnitude;
            _mainRigidbody.AddForce(forceVector, ForceMode.Impulse);
            State = FruitState.Thrown;
            _lifetimeLeft = _lifetimeSecs;
        }

        private void Awake()
        {
            TryGetComponent(out _mainRigidbody);
            TryGetComponent(out _sliceTrigger);
        }

        private void OnEnable()
        {
            GameState.GameRestarted += OnGameRestart;    
        }

        private void Update()
        {
            UpdateLifetime();
        }

        private void UpdateLifetime()
        {
            if (!(_state == FruitState.Sliced || _state == FruitState.Thrown)) return;

            _lifetimeLeft -= Time.deltaTime;
            if (_lifetimeLeft <= 0)
                Destroy(gameObject);
        }

        private void RefreshFruitModel()
        {
            switch (_state)
            {
                case FruitState.Initialized:
                case FruitState.Thrown:
                    _wholeModel.SetActive(true);
                    _slicedModel.SetActive(false);
                    _sliceTrigger.enabled = true;
                    break;
                case FruitState.Sliced:
                    _wholeModel.SetActive(false);
                    _slicedModel.SetActive(true);
                    _sliceTrigger.enabled = false;
                    break;
            }
        }

        private void AddForceForSlicedPart(Rigidbody body, Vector3 direction, Vector3 position, float force)
        {
            body.velocity = _mainRigidbody.velocity;
            body.angularVelocity = _mainRigidbody.angularVelocity;

            float directionAngle = Mathf.Acos(direction.x / direction.magnitude) * Mathf.Rad2Deg;
            if (direction.y < 0)
            {
                directionAngle = 360 - directionAngle;
            }
            float rightAngleToDirection = directionAngle + 90;
            body.AddForce(new Vector3(Mathf.Cos(rightAngleToDirection * Mathf.Deg2Rad), Mathf.Sin(rightAngleToDirection * Mathf.Deg2Rad)) * force, ForceMode.Impulse);
        }

        private void OnGameRestart()
        {
            State = FruitState.Sliced;
            Destroy(gameObject);
        }

        private void OnDisable()
        {
            GameState.GameRestarted -= OnGameRestart;
        }

        private void OnDestroy()
        {
            FruitDestroyed?.Invoke(this);
        }
    }
}